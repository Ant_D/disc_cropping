from circle import Circle


class CircleCropper:
    def __init__(self, margin_factor=0.05):
        self.margin_factor = margin_factor

    def crop_to_circle(self, image, circle):
        circle = self.__add_margin(circle)
        return image[
            circle[1] - circle.r : circle[1] + circle.r + 1,
            circle[0] - circle.r : circle[0] + circle.r + 1
        ]

    def __add_margin(self, circle):
        return Circle(
            circle.x,
            circle.y,
            int(round((1 + self.margin_factor) * circle.r))
        )