from collections import namedtuple


Circle = namedtuple('Circle', ['x', 'y', 'r'])