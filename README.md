# Disc cropping
Finds disc on image and crop it.

<img src="./example/input.jpg" width="200">
<img src="./example/output.jpg" width="200">

Usage:
`python crop_disc.py <input image file> [-o [output image file]] [-c [config file path]]`

Testing:
`python test.py [-c [config file path]]`

Tuning:
`python tune.py`