import cv2


def resize_max_width(image, max_width):
    h, w, *_ = image.shape
    if w > h:
        h = int(round((max_width / w) * h))
        w = max_width
    else:
        w = int(round((max_width / h) * w))
        h = max_width
    return cv2.resize(image, (w, h))