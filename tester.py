import math
import os
import sys
from collections import namedtuple
from statistics import mean

import cv2

from circle import Circle


Test = namedtuple('Test', ['image_path', 'circle_path'])


class Tester:
    def __init__(self, disc_finder, tests_dir='tests'):
        self.tests_dir = tests_dir
        self.disc_finder = disc_finder

    def test(self, verbose=0):
        tests = self.__get_tests()
        accuracies = []
        for test in tests:
            m = self.__run_test(test)
            accuracies.append(m)
            if verbose:
                print('{}: {}'.format(test.image_path, m), file=sys.stderr)
        return mean(accuracies)

    def __get_tests(self):
        image_paths = list(filter(lambda p: p.endswith('.jpg'), os.listdir(self.tests_dir)))
        image_paths = list(map(lambda p: os.path.join(self.tests_dir, p), image_paths))
        circle_paths = list(map(lambda s: s.replace('.jpg', '.txt'), image_paths))
        tests = [Test(image_path=ip, circle_path=cp) for ip, cp in zip(image_paths, circle_paths)]
        return tests

    def __run_test(self, test: Test):
        image = cv2.imread(test.image_path, cv2.IMREAD_COLOR)
        expected_circle = self.__read_circle(test.circle_path)
        disc = self.disc_finder.find(image)
        contour = disc.contour if disc else None
        return self.__calculate_accuracy(expected_circle, contour)

    @staticmethod
    def __read_circle(path):
        with open(path, 'r') as f:
            return Circle(*map(int, f.read().split()))

    @staticmethod
    def __calculate_accuracy(expected: Circle, gotten: Circle):
        return Tester.__intersection_area(expected, gotten) / Tester.__union_area(expected, gotten) \
            if gotten else 0

    @staticmethod
    def __intersection_area(c1: Circle, c2: Circle):
        min_x = max(c1.x - c1.r, c2.x - c2.r)
        max_x = min(c1.x + c1.r, c2.x + c2.r)
        min_y = max(c1.y - c1.r, c2.y - c2.r)
        max_y = min(c1.y + c1.r, c2.y + c2.r)
        return max(0, (max_x - min_x) * (max_y - min_y))

    @staticmethod
    def __union_area(c1: Circle, c2: Circle):
        return (4 * c1.r * c1.r) + (4 * c2.r * c2.r) - Tester.__intersection_area(c1, c2)