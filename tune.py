import random
from copy import deepcopy
import sys

import config
from circle_finder import CircleFinder
from disc_finder import DiscFinder
from tester import Tester


BEST_CONFIG_FILE_PATH = 'best_model.json'


def get_random_contour_and_hole_finders():
    min_radius_factor = random.uniform(0.1, 0.35)
    max_radius_factor = random.uniform(max(min_radius_factor, 0.4), 0.5)
    contour_finder = CircleFinder(
        min_dist_factor=random.uniform(0.01, 0.2),
        param1=random.randint(80, 120),
        param2=random.randint(30, 50),
        min_radius_factor=min_radius_factor,
        max_radius_factor=max_radius_factor,
        max_width_threshold=512
    )
    hole_finder = deepcopy(contour_finder)
    hole_finder.min_radius_factor *= 0.5
    hole_finder.max_radius_factor *= 0.5
    return contour_finder, hole_finder


def main():
    best_disc_finder = config.load(BEST_CONFIG_FILE_PATH)
    best_accuracy = Tester(best_disc_finder).test() if best_disc_finder else None
    print('Current best accuracy:', best_accuracy)
    while True:
        contour_finder, hole_finder = get_random_contour_and_hole_finders()
        disc_finder = DiscFinder(contour_finder, hole_finder)
        accuracy = Tester(disc_finder).test()
        if best_accuracy is None or best_accuracy < accuracy:
            best_accuracy = accuracy
            best_disc_finder = disc_finder
            config.save(best_disc_finder, BEST_CONFIG_FILE_PATH)
            print('Current best accuracy:', best_accuracy)


if __name__ == '__main__':
    main()