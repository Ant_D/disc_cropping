import argparse
import sys

import cv2

import config
from circle_cropper import CircleCropper
from circle_finder import CircleFinder
from disc_finder import Disc, DiscFinder
from utils import resize_max_width


MAX_DISPLAY_WIDTH = 512


def parse_args(args):
    parser = argparse.ArgumentParser(description='Detect and crop disc')
    parser.add_argument('input_path', type=str, help='Input image file path')
    parser.add_argument('--output', '-o', type=str, help='Output image file path')
    parser.add_argument('--config', '-c', type=str, help='Config file path')
    return parser.parse_args(args)


def main():
    args = parse_args(sys.argv[1:])
    disc_finder = config.load(args.config) if args.config \
        else DiscFinder(CircleFinder(), CircleFinder())

    image = cv2.imread(args.input_path, cv2.IMREAD_COLOR)
    disc = disc_finder.find(image)
    if disc:
        circle_cropper = CircleCropper()
        image = circle_cropper.crop_to_circle(image, disc.contour)

    if args.output:
        cv2.imwrite(args.output, image)
    else:
        image = resize_max_width(image, MAX_DISPLAY_WIDTH)
        cv2.imshow('cropped disc', image)
        cv2.waitKey(0)


if __name__ == '__main__':
    main()