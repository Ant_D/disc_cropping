import argparse
import sys

import config
from circle_finder import CircleFinder
from disc_finder import DiscFinder
from tester import Tester


def parse_args(args):
    parser = argparse.ArgumentParser(description='Test disc finder')
    parser.add_argument('--config', '-c', type=str, help='Config file path')
    return parser.parse_args(args)


if __name__ == '__main__':
    args = parse_args(sys.argv[1:])
    disc_finder = config.load(args.config) if args.config \
        else DiscFinder(CircleFinder(), CircleFinder())
    tester = Tester(disc_finder)
    accuracy = tester.test(verbose=1)
    print(accuracy)