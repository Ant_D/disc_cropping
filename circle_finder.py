import cv2
import numpy as np

from circle import Circle
from utils import resize_max_width


class CircleFinder:
    def __init__(
        self,
        min_dist_factor=0.1,
        param1=100,
        param2=60,
        min_radius_factor=0.1,
        max_radius_factor=0.5,
        max_width_threshold=512
    ):
        self.min_dist_factor = min_dist_factor
        self.param1 = param1
        self.param2 = param2
        self.min_radius_factor = min_radius_factor
        self.max_radius_factor = max_radius_factor
        self.max_width_threshold = max_width_threshold

    def find(self, image):
        if 0 in image.shape:
            return []
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = cv2.equalizeHist(image)
        factor = 1
        if self.max_width_threshold < max(image.shape):
            factor = self.max_width_threshold / max(image.shape)
            image = resize_max_width(image, self.max_width_threshold)
        image = cv2.medianBlur(image, 5)
        circles = cv2.HoughCircles(
            image,
            cv2.HOUGH_GRADIENT,
            dp=1,
            minDist=self.__get_min_dist(image.shape),
            param1=self.param1,
            param2=self.param2,
            minRadius=self.__get_min_radius(image.shape),
            maxRadius=self.__get_max_radius(image.shape)
        )
        if circles is not None and len(circles) > 0:
            circles = np.int32(np.around(circles / factor))[0, :]
            circles = list(map(lambda c: Circle(x=c[0], y=c[1], r=c[2]), circles))
        else:
            circles = []
        return circles

    def __get_min_radius(self, shape):
        return int(round(self.min_radius_factor * min(shape)))

    def __get_max_radius(self, shape):
        return int(round(self.max_radius_factor * min(shape)))

    def __get_min_dist(self, shape):
        return self.min_dist_factor * min(shape)