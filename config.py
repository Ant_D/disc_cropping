import os
import json
from circle import Circle

from circle_finder import CircleFinder
from disc_finder import DiscFinder


def save(disc_finder: DiscFinder, file_path):
    config = json.dumps(
        {
            'contour_finder': disc_finder.contour_finder.__dict__,
            'hole_finder': disc_finder.hole_finder.__dict__,
        },
        indent=4
    )
    with open(file_path, 'w', encoding='utf-8') as f:
        print(config, file=f)


def load(file_path):
    disc_finder = None
    if os.path.isfile(file_path):
        with open(file_path, 'r', encoding='utf-8') as f:
            config = json.load(f)
            disc_finder = DiscFinder(
                CircleFinder(**config['contour_finder']),
                CircleFinder(**config['hole_finder'])
            )
    return disc_finder