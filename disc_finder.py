import math
from collections import namedtuple

from circle import Circle
from circle_cropper import CircleCropper
from circle_finder import CircleFinder


Disc = namedtuple('Disc', ['contour', 'hole'])


class DiscFinder:
    def __init__(self, contour_finder: CircleFinder, hole_finder: CircleFinder):
        self.contour_finder = contour_finder
        self.hole_finder = hole_finder
        self.circle_cropper = CircleCropper(margin_factor=0.01)

    def find(self, image):
        contours = self.contour_finder.find(image)
        holes = self.hole_finder.find(image)
        candidates = []
        for contour in contours:
            hole = self.__find_hole_for_contour(contour, holes)
            candidates.append(Disc(contour=contour, hole=hole))
        disc = min(candidates, key=lambda d: self.__loss(d)) if candidates else None
        if not disc or not disc.hole:
            disc = max(candidates, key=lambda d: d.contour.r) if candidates else None
        return disc

    def __find_hole_for_contour(self, contour, holes):
        return min(holes, key=lambda h: DiscFinder.__dist(contour, h) / contour.r) if holes else None

    def __loss(self, disc):
        return self.__dist(disc.contour, disc.hole) if disc.hole else math.inf

    @staticmethod
    def __dist(c1: Circle, c2: Circle):
        dx = c1.x - c2.x
        dy = c1.y - c2.y
        return math.sqrt(dx * dx + dy * dy)
